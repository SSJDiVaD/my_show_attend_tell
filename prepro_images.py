"""Run the encoder (as described in 4.3) on all images beforehand. Assumes
you've already run prepro_captions beforehand.

This module uses CUDA.
"""

import gc  # Python garbage collector used to reduce MemoryErrors?
import hickle
import numpy as np
import os
import scipy.ndimage
from skimage import transform
# import sys
import torch
from torch import nn
from torch.autograd import Variable
# from torch.utils.data import TensorDataset, DataLoader
import torchvision

import const
import utils


class Encoder(nn.Module):
    """Just the feature extractor from VGG19. All weights are fixed so that no
    training is possible.

    Depending on const.VGG_USE_BN, will use Batch Normalization (or not)

    The behaviour of this nn is the encoder as described in 4.3
    """
    def __init__(self):
        super(Encoder, self).__init__()
        if const.VGG_USE_BN:
            vgg = torchvision.models.vgg19_bn(pretrained=True)
        else:
            vgg = torchvision.models.vgg19(pretrained=True)
        for param in vgg.parameters():
            param.requires_grad = False

        # chop off the last max pool (note that the last layer is the same,
        # regardless of const.USE_BNORM)
        self.features = nn.Sequential(
            *list(vgg.features.children())[:-1])

        if const.USE_CUDA:
            self.features = self.features.cuda()

    def forward(self, x):
        return self.features(x)


def npimg2torchimg(imgs):
    """Given either:
        1) a list of numpy images imgs in the format (Nx(HxWxC))
        2) a numpy array of images in the form NxHxWxC
    return a numpy array of format NxCxHxW
    """
    hwc = imgs[0].shape
    assert hwc == const.PREPRO_IMAGE_SHAPE
    assert(all(img.shape == hwc for img in imgs))
    imgs = np.asarray(imgs)
    return np.transpose(imgs, (0, 3, 1, 2))


def resize_and_crop(img):
    """per 4.3, resize img such that shortest side is 256 and then center-crop
    img to 224x224. Assume img is a np.ndarray of HxWx3."""
    # resize
    if img.shape[0] < img.shape[1]:
        output_shape = (const.PREPRO_SHORTEST_SIDE,
                        utils.round_int(const.PREPRO_SHORTEST_SIDE / img.shape[0] * img.shape[1]),
                        img.shape[2])
    else:
        output_shape = (utils.round_int(const.PREPRO_SHORTEST_SIDE / img.shape[1] * img.shape[0]),
                        const.PREPRO_SHORTEST_SIDE,
                        img.shape[2])
    img = transform.resize(img, output_shape, mode='reflect',
                           preserve_range=True, anti_aliasing=True)

    # crop
    H, W, _ = img.shape
    assert (H == const.PREPRO_SHORTEST_SIDE and W >= const.PREPRO_SHORTEST_SIDE
            or H >= const.PREPRO_SHORTEST_SIDE and W == const.PREPRO_SHORTEST_SIDE)
    left = utils.round_int((W - const.PREPRO_FINAL_SIDE) / 2.)
    right = left + const.PREPRO_FINAL_SIDE
    top = utils.round_int((H - const.PREPRO_FINAL_SIDE) / 2.)
    bottom = top + const.PREPRO_FINAL_SIDE
    img = img[top:bottom, left:right, :]
    assert img.shape == const.PREPRO_IMAGE_SHAPE
    return img

    # # following Torch implementation requires PIL images
    # img = torchvision.transforms.Resize(const.PREPRO_SHORTEST_SIDE)(img)
    # img = torchvision.transforms.CenterCrop(const.PREPRO_FINAL_SIZE)(img)
    # assert img.shape == const.PREPRO_IMAGE_SHAPE
    # return img


def gray2rgb(img):
    """Given a numpy image that is grayscale (HxW), triplicate it to RGB
    (HxWx3)"""
    w, h = img.shape
    result = np.empty((w, h, 3), dtype=img.dtype)
    result[:, :, 2] = result[:, :, 1] = result[:, :, 0] = img
    assert result.shape == (w, h, 3)
    return result


def process_images_flickr8k():
    """resize such that the shortest side is 256 and center-crop images to
    224x224x3 as per 5.4, then pass through the VGG feature detector as per 4.3,
    then save features.

    The resulting hickle is a tuple:
        -img_filenames (dict of str to int), a dict telling us which image
        filename corresponds to which set of features
        -all_features (numpy ndarray of dimension num_images x const.L x const.D)
    """

    encoder = Encoder()
    # per pytorch documentation on pretrained models, I need to normalize images
    # as follows
    img_normalizer = torchvision.transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                                      std=[0.229, 0.224, 0.225])
    image_files = os.listdir(const.FLICKR8K_IMG_DIR)
    if const.PREPRO_LIMIT is not None and 0 <= const.PREPRO_LIMIT < len(image_files):
        print('for testing purposes, abbreviating split to size', const.PREPRO_LIMIT)
        image_files = image_files[:const.PREPRO_LIMIT]
    num_images = len(image_files)
    image_files = iter(image_files)
    gc.collect()
    all_features = np.ndarray([num_images, const.L, const.D], dtype=np.float32)

    img_filenames = []
    # process in batches
    for batch_i, start in enumerate(range(0, num_images, const.PREPRO_BATCH_SIZE)):
        with utils.Timer('batch'):
            end = min(start + const.PREPRO_BATCH_SIZE, num_images)
            this_batch_size = end - start
            imgs = []
            for j in range(this_batch_size):
                image_file = next(image_files)
                img = scipy.ndimage.imread(os.path.join(const.FLICKR8K_IMG_DIR, image_file), mode='F')
                if len(img.shape) == 2:  # grayscale; convert to RGB
                    img = gray2rgb(img)
                img = resize_and_crop(img)
                imgs.append(img)
                img_filenames.append(image_file)

            # construct a torch tensor out of imgs and pass it through the nn
            imgs = torch.from_numpy(npimg2torchimg(imgs)).float()
            if const.USE_CUDA:
                imgs = imgs.cuda(async=True)
            for i in range(len(imgs)):
                imgs[i, :, :, :] = img_normalizer(imgs[i, :, :, :])
            imgs = Variable(imgs, requires_grad=False, volatile=True)
            imgs = encoder(imgs)
            imgs = imgs.cpu().data.numpy()
            imgs = imgs.reshape(this_batch_size, const.D, const.L)
            imgs = np.transpose(imgs, axes=(0, 2, 1))  # started in NxDxL; paper wants NxLxD
            all_features[start:end, :, :] = imgs
            print('Assembled images: {0}/{1}'.format(end, num_images))
    # convert img_filenames into a dict of filename to index
    img_filenames = dict((filename, i) for i, filename in enumerate(img_filenames))

    # use hickle to save huge feature vectors
    filename = const.FLICKR8K_FEATURES_FILENAME
    hickle.dump((img_filenames, all_features), filename)
    print('Saved to', filename)


# def process_images_coco():
#     """
#     UNUSED as of April 6, 2018
#     Process the images (using Encoder) as generated by resize.py from:
#     https://github.com/yunjey/show-attend-and-tell
#     """
#     extractor = Encoder()
#     splits = ['train', 'val']
#     for split in splits:
#         resized_folder = './image/{0}2014_resized/'.format(split)
#         image_files = os.listdir(resized_folder)
#         if const.PREPRO_LIMIT is not None and const.PREPRO_LIMIT < len(image_files):
#             print('for testing purposes, abbreviating split to size', const.PREPRO_LIMIT)
#             image_files = image_files[:const.PREPRO_LIMIT]
#         num_images = len(image_files)
#         image_files = iter(image_files)
#         gc.collect()  # the next step takes up a ton of memory, so *fingers crossed*
#         all_features = np.ndarray([num_images, 512, 196], dtype=np.float32)  # dimensions are hard-coded as per 4.3
#         # of the paper
#
#         # process in batches
#         for batch_i, start in enumerate(range(0, num_images, const.PREPRO_BATCH_SIZE)):
#             with utils.Timer('batch'):
#                 end = min(start + const.PREPRO_BATCH_SIZE, num_images)
#                 this_batch_size = end - start
#                 imgs = []
#                 for j in range(this_batch_size):
#                     image_file = next(image_files)
#                     img = scipy.ndimage.imread(os.path.join(resized_folder, image_file), mode='RGB')
#                     if img.shape != const.IMAGE_SHAPE:
#                         print('error: wrong image dimensions:', img.shape, 'in file:', image_file, file=sys.stderr)
#                         assert False
#                     imgs.append(img)
#                 imgs = torch.from_numpy(npimg2torchimg(imgs)).float()
#                 if const.USE_CUDA:
#                     imgs = imgs.cuda(async=True)
#                 imgs = Variable(imgs, requires_grad=False)
#                 imgs = extractor(imgs)
#                 imgs = imgs.cpu().data.numpy()
#                 imgs = imgs.reshape(this_batch_size, 512, 196)  # these dimensions are hard-coded as described in
#                 #  4.3
#                 all_features[start:end, :, :] = imgs
#                 print('Assembled images: {0}/{1}'.format(end, num_images))
#
#         # use hickle to save huge feature vectors
#         filename = const.PREPRO_FILENAME.format(split)
#         hickle.dump(all_features, filename)
#         print('Saved to', filename)
#         pass


if __name__ == '__main__':
    process_images_flickr8k()
