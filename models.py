"""The LSTM model from the paper, along with its supporting sub-networks."""
import torch
from torch.autograd import Variable
from torch import nn

import const
import utils


class InitMlp(nn.Module):
    """Basic fully connected neural network used to perform initialization for
    c_0 and h_0. Because I don't have a lot of information about how the paper
    initializes their states, I'm basically just taking a guess here.

    This implementation is a basic input-hidden-hidden-output network
    """
    def __init__(self, amplitude=1):
        """The final activation function is a tanh, which would normally output
        values between -1 and 1. Optional parameter amplitude is either 1 or 2,
        representing an output range of (-1, 1) or (-2, 2)
        """
        super(InitMlp, self).__init__()
        self.first = nn.Sequential(
            nn.Linear(const.D, const.BASIC_MLP_HIDDEN_SIZE),
            nn.LeakyReLU(),
            nn.BatchNorm1d(const.BASIC_MLP_HIDDEN_SIZE),
            nn.Dropout(const.MLP_DROPOUT_PROB),
        )
        # self.second = nn.Sequential(
        #     nn.Linear(const.BASIC_MLP_HIDDEN_SIZE, const.BASIC_MLP_HIDDEN_SIZE),
        #     nn.LeakyReLU(),
        #     nn.BatchNorm1d(const.BASIC_MLP_HIDDEN_SIZE),
        # )
        assert amplitude == 1 or amplitude == 2
        self.last = nn.Sequential(
            nn.Linear(const.BASIC_MLP_HIDDEN_SIZE, const.n * amplitude),
            # I puzzled over what activation function to use here. But since c_t
            # and h_t both are composed of quantities which are between -1 and 1
            # (in the case of c_t, -2 and 2), I think tanh
            # makes the most sense
            nn.BatchNorm1d(const.n * amplitude),
            nn.Dropout(const.MLP_DROPOUT_PROB),
            nn.Tanh(),
        )
        self.amplitude = amplitude

    def forward(self, a_is_average):
        """a_is_average should be a minibatch of features (dim
        batch_size x const.D)

        Returns a vector of size (batch_size x const.n).
        """
        batch_size = a_is_average.shape[0]
        assert a_is_average.shape == (batch_size, const.D)
        out = self.first(a_is_average)
        # out = self.second(out)
        out = self.last(out)
        # out *= self.amplitude  # note: auto-differentiation can't handle *=
        # out = out * self.amplitude
        assert self.amplitude == 1 or self.amplitude == 2
        if self.amplitude == 2:
            out = out[:, :const.n] + out[:, const.n:]  # split and sum the output
        assert out.shape == (batch_size, const.n)
        return out


class AttentionModel(nn.Module):
    """The Attention model f_att from 3.1.2. As with InitMlp, I'm taking a
    guess as to a good topology because the paper is unspecific.

    Currently, going with input-hidden-hidden-output.
    """

    def __init__(self):
        super(AttentionModel, self).__init__()
        self.first = nn.Sequential(
            nn.Linear(const.D + const.n, const.F_ATT_HIDDEN_SIZE),
            nn.LeakyReLU(),
            # nn.BatchNorm1d(const.F_ATT_HIDDEN_SIZE),
            nn.Dropout(const.F_ATT_DROPOUT_PROB),
        )
        # half_size = utils.round_int(const.F_ATT_HIDDEN_SIZE / 2)
        # self.second = nn.Sequential(
        #     nn.Linear(const.F_ATT_HIDDEN_SIZE, half_size),
        #     nn.LeakyReLU(),
        #     nn.BatchNorm1d(half_size),
        # )
        # the final layer doesn't need an activation function because it's gonna
        # get softmaxed when we calculate alpha_ti.
        # self.last = nn.Linear(half_size, 1)
        self.last = nn.Linear(const.F_ATT_HIDDEN_SIZE, 1)
        # TODO attention model should look at whole image at a time?

    def forward(self, x):
        """x is a tuple of:
            -a_i, a mini-batch of feature vectors a_i of length D
            -h_t1, a mini-batch of h_{t-1}, the hidden state from the previous
        iteration. Each h_{t-1} is a vector of length n.

        return value is a mini batch of scalars (batch_size x 1)
        """
        a_i, h_t1 = x
        batch_size = a_i.shape[0]
        assert a_i.shape == (batch_size, const.D)
        assert h_t1.shape == (batch_size, const.n)
        out = torch.cat((a_i, h_t1), dim=1)
        out = self.first(out)
        # out = self.second(out)
        out = self.last(out)
        assert out.shape == (batch_size, 1)
        return out


class DeepOutputLayer(nn.Module):
    """The deep output layer per 3.1.2 eq 7"""

    def __init__(self, K):
        """K is per 3.1.1: the size of the vocabulary"""
        super(DeepOutputLayer, self).__init__()
        self.K = K
        # self.L_o_normalizer = nn.BatchNorm1d(const.m)
        self.L_o = nn.Sequential(nn.Dropout(const.OUTPUT_DROPOUT_PROB),
                                 nn.Linear(const.m, K),)
        self.L_h = nn.Sequential(nn.Dropout(const.OUTPUT_DROPOUT_PROB),
                                 nn.Linear(const.n, const.m))
        self.L_z = nn.Sequential(nn.Dropout(const.OUTPUT_DROPOUT_PROB),
                                 nn.Linear(const.D, const.m))
        # self.y_t_softmaxer = nn.Softmax(dim=1)
        # self.y_t_softmaxer = nn.LogSoftmax(dim=1)  # we're doing NLLLoss, so we
        # need to finish with a LogSoftmax
        # ACTUALLY, we don't need a softmaxer because CrossEntropyLoss takes
        # care of it for us.

    def forward(self, x):
        """x is a tuple:
            -Ey_t1: Ey_{t-1} per 3.1.2, the embedding matrix E applied to the
            previously selected word y_{t-1}. Should have dimensionality
            (batch_size x const.m)
            -h_t: the hidden state per 3.1.2 eq 3. Should have dimensionality
            (batch_size x const.n)
            -z_t: the context vector per 3.1.2 eq 6. Should have dimensionality
            (batch_size x const.D)

        Implementation is as per 3.1.2 eq 7

        Output y_t is of dimension (batch_size x self.K), where
        K is the vocabulary size.
        """
        Ey_t1, h_t, z_t = x
        batch_size = Ey_t1.shape[0]
        assert Ey_t1.shape == (batch_size, const.m)
        assert h_t.shape == (batch_size, const.n)
        assert z_t.shape == (batch_size, const.D)
        out = Ey_t1 + self.L_h(h_t) + self.L_z(z_t)
        assert out.shape == (batch_size, const.m)
        # out = self.L_o_normalizer(out)  # my idea
        out = self.L_o(out)
        # out = self.y_t_softmaxer(out)
        assert out.shape == (batch_size, self.K)
        return out


class Decoder(nn.Module):
    """Customized LSTM network per 3.1.2"""

    def __init__(self, K):
        """Params:
            -K is per 3.1.1: the size of the vocabulary
        """
        super(Decoder, self).__init__()
        # the MLP used to process the image to create the initial cell state
        self.mlp_c = InitMlp(amplitude=2)
        # the MLP used to process the image to create the initial hidden state
        self.mlp_h = InitMlp()
        self.K = K
        self.y_embedder = nn.Embedding(K, const.m)  # matrix E per 3.1.2 eq 1
        # self.cell_to_cell = nn.Sequential(nn.Linear(const.n, const.n),
        #                                   nn.Tanh(),
        #                                   )
        self.hidden_to_hidden = nn.Sequential(nn.Linear(const.n, const.n),
                                              nn.Tanh(),
                                              )
        self.f_att = AttentionModel()  # f_att attention mechanism per 3.1.2 eq 4, 4.2
        self.alpha_softmaxer = nn.Softmax(dim=1)  # used to calculate alpha_ti per 3.1.2 eq 5
        # self.input_normalizer = nn.BatchNorm1d(const.m + const.D)
        self.beta_gater = nn.Sequential(
            nn.Linear(const.n, const.F_BETA_HIDDEN_SIZE),
            nn.LeakyReLU(),
            nn.Dropout(p=const.F_BETA_DROPOUT_PROB),
            nn.Linear(const.F_BETA_HIDDEN_SIZE, const.F_BETA_HIDDEN_SIZE // 2),
            nn.LeakyReLU(),
            nn.Dropout(p=const.F_BETA_DROPOUT_PROB),
            nn.Linear(const.F_BETA_HIDDEN_SIZE // 2, 1),
            nn.Sigmoid(),
        )
        self.input_dropper = nn.Dropout(p=const.LSTM_DROPOUT_PROB)
        input_size = const.m + const.D  # per 3.1.2 eq 1. However, we don't
        # include const.n in here because h_{t-1} gets passed separately to
        # LSTMCell
        hidden_size = const.n
        self.rnn_cell = nn.LSTMCell(input_size, hidden_size)
        self.deep_output_layer = DeepOutputLayer(K)  # 3.1.2 eq 7
        if const.USE_CUDA:
            self.mlp_c = self.mlp_c.cuda()
            self.mlp_h = self.mlp_h.cuda()
            self.y_embedder = self.y_embedder.cuda()
            self.hidden_to_hidden = self.hidden_to_hidden.cuda()
            # self.cell_to_cell = self.cell_to_cell.cuda()
            self.f_att = self.f_att.cuda()
            self.alpha_softmaxer = self.alpha_softmaxer.cuda()
            self.beta_gater = self.beta_gater.cuda()
            # self.input_normalizer = self.input_normalizer.cuda()
            self.input_dropper = self.input_dropper.cuda()
            self.rnn_cell = self.rnn_cell.cuda()
            self.deep_output_layer = self.deep_output_layer.cuda()

    @staticmethod
    def phi_soft(a_is, alpha_is, beta):
        """An implementation of the soft phi per 3.1.2 eq 6 and 4.2 eq 13
        Params:
            -a_is is a mini-batch of L vectors of length D
            (ie. batch_size x L x D tensor)
            -alpha_is is a mini-batch of L vectors
            (ie. batch_size x L tensor)
            -beta is the gating scalar described in 4.2.1

        Return value is z_t (ie. batch_size x D tensor)
        """
        batch_size = a_is.shape[0]
        assert a_is.shape == (batch_size, const.L, const.D)
        # if alpha_is doesn't have the last x1 dimension, add it
        if len(alpha_is.shape) == 2:
            alpha_is = torch.unsqueeze(alpha_is, -1)
        assert alpha_is.shape == (batch_size, const.L, 1)
        result = torch.squeeze(torch.matmul(torch.transpose(a_is, dim0=1, dim1=2), alpha_is), 2)
        result = result * beta
        assert result.shape == (batch_size, const.D)
        return result

    def forward(self, a_is, target_length, word_to_idx):
        """Params:
            -a_is, a mini-batch of feature vectors a, where each a has
            dimensionality LxD (ie. dimensionality should be batch_size
            x L x D).
            -target_length is the length of the caption we'd like to generate
            (including tags such as <START>, <END>, or <NULL>).
            -word_to_idx as generated by prepro_captions

        Return value is a tuple:
            -prob_ys has dimension (batch_size x target_length x K)
            -alpha_ts has dimension (batch_size x (target_length - 1) x const.L)
        """
        batch_size = a_is.shape[0]
        assert a_is.shape == (batch_size, const.L, const.D)
        assert len(word_to_idx) == self.K
        # our ultimate goal is to calculate prob_ys, the probability of each
        # word in a given position
        prob_ys = []

        # t == 0
        a_is_average = torch.mean(a_is, dim=1)
        assert a_is_average.shape == (batch_size, const.D)
        h_t = self.mlp_h(a_is_average)
        c_t = self.mlp_c(a_is_average)  # initialize c_0 and h_0
        prob_y = torch.zeros((batch_size, self.K))
        prob_y[:, word_to_idx['<START>']] = 1  # every sentence starts with <START>
        if const.USE_CUDA:
            prob_y = prob_y.cuda()
        prob_y = Variable(prob_y, requires_grad=False)
        prob_ys.append(prob_y)

        # t > 0
        alpha_ts = []
        for t in range(1, target_length):
            curr_prob_ys = prob_ys[t - 1]
            assert curr_prob_ys.shape == (batch_size, self.K)
            # generate the inputs to the LSTM from 3.1.2, equation (1)
            _, y_t1 = torch.max(curr_prob_ys, dim=1)  # pick the y_{t-1} with the highest probability
            Ey_t1 = self.y_embedder(y_t1)  # put y_{t-1} through an embedding layer
            h_t = self.hidden_to_hidden(h_t)
            # c_t = self.cell_to_cell(c_t)
            # c_t = c_t * 2

            # handle the attention mechanism
            e_t = []
            for i in range(const.L):
                e_t.append(self.f_att((a_is[:, i, :], h_t)))
            e_t = torch.cat(e_t, dim=1)
            alpha_t = self.alpha_softmaxer(e_t)
            assert alpha_t.shape == e_t.shape == (batch_size, const.L)
            alpha_ts.append(alpha_t)
            beta = self.beta_gater(h_t)
            z_t = self.phi_soft(a_is, alpha_t, beta)

            # run everything through an RNN w/ a deep output layer
            # note that, when running the data into the RNN, we only use Ey_t1
            # and z_t because h_{t-1} is gonna be passed separately with c_t
            assert Ey_t1.shape == (batch_size, const.m)
            # assert h_t1.shape == (batch_size, const.n)
            assert z_t.shape == (batch_size, const.D)
            rnn_input = torch.cat((Ey_t1, z_t), dim=1)
            # rnn_input = self.input_normalizer(rnn_input)  # my own idea: batch normalization
            # per the paper by Zaremba, Sutskever, and Vinyals, apply dropout to the RNN input.
            rnn_input = self.input_dropper(rnn_input)
            assert rnn_input.shape == (batch_size, const.m + const.D)
            h_t, c_t = self.rnn_cell(rnn_input, (h_t, c_t))
            prob_ys.append(self.deep_output_layer((Ey_t1, h_t, z_t)))

        prob_ys = torch.stack(prob_ys, dim=1)
        assert prob_ys.shape == (batch_size, target_length, self.K)
        alpha_ts = torch.stack(alpha_ts, dim=1)
        assert alpha_ts.shape == (batch_size, target_length - 1, const.L)
        return prob_ys, alpha_ts
