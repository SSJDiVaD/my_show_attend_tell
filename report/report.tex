\documentclass[a4paper]{article}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage{subfig}

\usepackage{hyperref}
\hypersetup{}

\graphicspath{{images/}}

\title{Dog Dog Dog: a re-implementation of Show, Attend and Tell}

\author{David Szeto}

\date{\today}

\begin{document}
\maketitle

\begin{abstract}
This report details my (partially successful) efforts to implement significant parts of the paper \emph{Show, Attend and Tell} by Xu, Ba, Kiros, Cho, Courville, Salakhutdinov, Zemel, and Bengio. I built a (partially) working implementation of every aspect of the model in the paper, using only the soft attention mechanism and training/testing on the Flickr8k dataset using the METEOR metric.
\end{abstract}

\section{Introduction}
\paragraph{}
The paper \emph{Show, Attend and Tell: Neural Image Caption Generation with Visual Attention} by Kelvin Xu, Jimmy Lei Ba, Ryan Kiros, Kyunghyun Cho, Aaron Courville, Ruslan Salakhutdinov, Richard S. Zemel, and Yoshua Bengio (which I will refer to in this report as simply "the original paper") describes a model which automatically generates captions for images\cite{paper}.
This model uses a common approach\cite{rnn}\cite{lstm_paper1}\cite{lstm_paper2} of using a recurrent neural network (specifically, a Long Short Term Memory\cite{lstm_origin}, or LSTM, network) for the caption generation task, but employs an additional \emph{attention model}\cite{soft_attention} to examine only part of an image at a time.
This allows the LSTM to focus on selecting a word to describe that particular location.
The innovations presented in the original paper allowed the new model to beat then-state-of-the-art scores on caption generation for the datasets Flickr8k\cite{flickr8k}, Flickr30k\cite{flickr30k}, and COCO\cite{coco}.
\paragraph{}
This report details my re-implementation of significant parts of the original paper in the Python framework Pytorch.\cite{pytorch}

\section{Compromises}
\label{sec:compromises}
\paragraph{}
I did not implement every aspect of the original paper. Here I will discuss the omissions.
\subsection{Attention model}
\label{subsec:compr_attn}
\paragraph{}
The original paper describes two attention models: a hard one, and a soft one.
The hard model stochastically selects a single location to which to pay attention, while the soft one simultaneously pays attention to every location to varying degrees.
These models are detailed in 4.1 and 4.2 from the original paper respectively.
\paragraph{}
I only implemented the soft attention model.
Of the two, I feel like this model might have more potential to perform better because it doesn't lock in the attention into a single location, which allows the model to examine multiple parts of an image at any given time.
This has the advantage that, if a particular segment of the image is too big to fit into one location, the soft model can observe the entire segment.
\subsection{Datasets}
\paragraph{}
Where the original paper trains and evaluates on three datasets (Flickr8k, Flickr30k, and COCO), my implementation only trains and evaluates on Flickr8k.
I did this because I had limited training time and hard disk space on the UofT computation servers.
\subsection{Evaluation metrics}
\label{subsec:meteor}
\paragraph{}
Where the original paper evaluates using two metrics (BLEU\cite{bleu} and METEOR\cite{meteor}), I only evaluated using METEOR.

\section{Preprocessing}
\label{sec:algo}
\paragraph{}
Here, I discuss the preprocessing I did to the Flickr8k dataset in order to prepare it for training and evaluation with my model. 

\subsection{Preprocessing of the images}
\subsubsection{Resize and center-crop}
\paragraph{}
Per 5.4 from the original paper, all images were first resized such that their shortest side was 256 pixels long, then center-cropped to 224$\times$224 pixels. The resizing used linear spline interpolation.
\paragraph{}
Some grayscale-only images were present alongside full-colour images in the Flickr8k dataset. These were converted to RGB images by simply taking the intensity and applying it to all three colour channels.
\subsubsection{Encoder}
\paragraph{}
As per 3.1.1 from the original paper, I ran each image through a fixed feature extractor (the original paper also calls this an \emph{Encoder}) in order to come up with a $L \times D$ set of \emph{attention vectors} each, where $L$ is the number of vectors \footnote{According to the dimensions of the final layer of the feature extractor, this was $L = 196 = 14 \times 14$.} and $D$ was the length of each feature \footnote{This was the number of activation maps in the final layer of my feature extractor, which was $D = 512$.}.
The original paper calls these attention vectors $\boldsymbol{a}_i, i \in [1, L]$.
These features were then run through my implementation of the LSTM decoder.
\paragraph{}
As per 4.3 from the original paper, the feature extractor was the VGG-19\cite{vgg} network minus the final fully connected layers and the last max pooling layer.
\paragraph{}
However, PyTorch offered two pre-trained versions of VGG-19\cite{pytorch_vgg}: one with the original architecture, and one with added batch normalizations\cite{bnorm}.
The one with the batch normalizations was claimed by the documentation to perform better by a percentage point or two on the ImageNet\cite{imagenet} challenge, so I chose to use that version instead of the original.

\subsection{Preprocessing of text}

\subsubsection{Punctuation and other symbols}
\paragraph{}
In their code's\cite{arctic} processing of the datasets' captions, the authors ignored the following punctuation/symbols:
\begin{itemize}
	\item . (period)
	\item , (comma)
	\item ' (single quote)
	\item " (double quote)
	\item ( (open bracket)
	\item ) (close bracket)
\end{itemize}
In addition, they made the following replacements:
\begin{itemize}
	\item '\&' (ampersand) was replaced with 'and'
	\item '-' (hyphen/dash) was replaced with ' ' (space)
\end{itemize}

\subsubsection{Maximum caption length}
\paragraph{}
The Flickr8k dataset contains captions as short as length 1, and as long as length 37. Right off the bat, I decided to ignore captions that only contained one word, because a large fraction of them seemed erroneous (eg. many captions were simply "a").
I also decided to ignore captions of length 2, because I felt that they lacked sufficient detail to make good training or testing examples, eg. "dog runs".
I also decided to ignore captions longer than 20 words\footnote{This was an arbitrary choice. I could probably have went a bit higher or lower without significantly affecting the results. I chose 20 because there were about the same number of 20-word captions as there were 3-word captions. Not a strong reason, but again, it's quite an arbitrary choice.}.
I felt that there weren't enough of these captions to make good training mini-batches, and they were too verbose.
Thus, I only examined captions of lengths between 3 and 20 (inclusive) for both training and testing.

\subsubsection{Word-to-index}
\label{subsubsec:word2idx}
\paragraph{}
Per 3.1.1 from the original paper, the authors represented each caption as a vector, with each word being represented by a component of the vector.
Upon examination of their code, I determined that they represented words numerically using a word-to-index map: a function assigning each distinct word in the dataset to a unique natural number.

\subsubsection{Special "words"}
\paragraph{}
I added three special symbols to the word-to-index map:
\begin{itemize}
	\item \textless START\textgreater: Prepended to every ground truth and generated caption but was not involved in back-propagation or any other stage of training.
	\item \textless END\textgreater: Appended to every training ground truth caption, but was otherwise treated like any other word for the purposes of training. During evaluation, captions were treated as if they ended immediately before the first occurence (if any) of this symbol.
	\item \textless NULL\textgreater: Unused in practice. Originally I intended to use this symbol to pad out captions which were too short.
\end{itemize}

\section{Encoder}
\paragraph{}
the original paper uses what they call their \emph{Encoder} in order to generate captions for a (preprocessed) image.
The backbone of the Encoder is an LSTM\cite{lstm_origin} cell.
\paragraph{}
In this section, I will describe my implementation of the Encoder.
As per its description in 3.1.2 from the original paper, it consists of the initializers, the attention model, the LSTM, and the deep output layer.

\subsection{Initializers}
\label{subsec:initMlp}
\paragraph{}
Typically, for caption generation, an LSTM iterates over a given number of steps and generates one word of the caption per step.
Between steps, it passes its \emph{hidden state} (called $\boldsymbol{h}_t$, where $t$ is the step number) and \emph{cell state} (called $\boldsymbol{c}_t$) back to itself.
These states allow the LSTM to employ a \emph{memory} from one word to the next.
\paragraph{}
Before the first iteration, in order to use an LSTM, one needs to generate the initial states (called $\boldsymbol{h}_0$ and $\boldsymbol{c}_0$).
One common approach is to initialize these states to have zero values, while another uses random values\cite{init}.
The original paper uses two multi-layer perceptrons (MLP) to generate each of $\boldsymbol{h}_0$ and $\boldsymbol{c}_0$, as described in 3.1.2 from the original paper.
It passes the mean of all of the attention vectors for any given image (which were generated by the Encoder) through the two MLPs.
\paragraph{}
The original paper completely fails to describe the architectures of these MLPs, so I created my own architecture.
Both of my MLPs have the same architecture:
\begin{enumerate}
	\item Linear
	\item Leaky ReLU
	\item Batch normalization
	\item Dropout\cite{dropout}
	\item Linear
	\item Batch normalization
	\item Dropout
	\item Hyperbolic tangent
\end{enumerate}
\paragraph{}
I chose the final activation layer of hyperbolic tangent because the LSTM expects the states to be bounded between -1 and 1.
\paragraph{}
Note that my MLP architecture is not intended to be a guess for the original architecture.
It is merely an attempt to be a guess for \emph{an} effective MLP.

\subsection{Attention model}
\subsubsection{Background}
\paragraph{}
At each step $t$, we calculate one of the inputs to the LSTM cell, called a \emph{context vector}, which the original paper calls $\boldsymbol{\hat{z}}_t$. 
%TODO as and zs must be bold
Vector $\boldsymbol{\hat{z}}_t$ is generated by the \emph{attention mechanism}.
\paragraph{}
A multi-layer perceptron (MLP) named $f_{att}$, called \emph{the attention model}, analyzes  a single attention vector $\boldsymbol{a}_i, i \in [1, L]$ at a time. It takes in $\boldsymbol{a}_i$ and the previous hidden state $\boldsymbol{h}_{t-1}$ and assigns a score, $e_{ti}$ to each attention vector $\boldsymbol{a}_i$. This $e_{ti}$ score represents (loosely) how much to pay attention to a given location.
These scores are put through a softmax function (with respect to $i$) in order to obtain the weights $\alpha_{ti}$.
\paragraph{}
According to the soft attention model defined in 4.2 from the original paper, the $\alpha_{ti}$ scores represent the fraction of attention to pay towards each attention location at any given moment.
The attention vectors $\boldsymbol{\hat{z}}_t$ are calculated according to $\boldsymbol{\hat{z}}_t = \phi(\{\boldsymbol{a}_i\}, \{\alpha_i\}) = \sum_i^L{\alpha_i\boldsymbol{a}_i}$
\paragraph{}
As with the other MLPs in the original paper, the authors did not give a specific architecture for the attention model $f_{att}$. Thus, I again had to improvise. I came up with the following architecture:
\begin{enumerate}
	\item Concatenate $\boldsymbol{a}_i$ and $\boldsymbol{h}_{t-1}$
	\item Linear
	\item Leaky ReLU
	\item Dropout
	\item Linear
\end{enumerate}
\paragraph{}
As with \ref{subsec:initMlp}, I wasn't trying to take a guess as to the authors' original architecture.
I was merely trying to guess an architecture that would work.
\paragraph{}
Note that I don't apply an activation function after the last layer. This is because the value coming out of $f_{att}$ will be sent through a softmax function in the calculation of $e_{ti}$, so such an activation function would be unnecessary.

\subsection{LSTM}
\paragraph{}
Per 3.1.2 from the original paper, at a given time step $t$, the authors' LSTM takes the following inputs:
\begin{itemize}
	\item an embedded version of the previously selected word $\boldsymbol{Ey}_{t-1}$, where $\boldsymbol{E}$ is a learned embedding matrix\cite{embedding}
	\item the context vector $\boldsymbol{\hat{z}}_t$ as calculated by the attention model
	\item the hidden and cell states of the previous iteration $\boldsymbol{h}_{t-1}$ and $\boldsymbol{c}_{t-1}$ respectively
\end{itemize}
The LSTM gives as ouput the new hidden and cell states $\boldsymbol{h}_t$ and $\boldsymbol{c}_t$ respectively.
\paragraph{}
The authors apply dropout to the non-state inputs (ie. $\boldsymbol{Ey}_{t-1}$ and $\boldsymbol{\hat{z}}_t$) according to the recommendations by (\cite{dropout_input}).
\paragraph{}
In my implementation of the LSTM part of the Decoder, I used the LSTMCell\footnote{If I had used PyTorch's LSTM instead of the LSTMCell, I wouldn't have had the ability to implement the attention mechanism} implementation provided by PyTorch. Just as the authors did, I applied a dropout to the non-state inputs.
\paragraph{}
I also added a hidden-to-hidden transition as described by \cite{h2h}.
This transition was simply a linear layer followed by a hyperbolic tangent activation function.
\paragraph{}
I chose a LSTM dimensionality\footnote{The size of the hidden and cell states} of 1536.

\subsection{Deep output layer}
\paragraph{}
A deep output layer was used to finally predict the likelihood of a given word $\boldsymbol{y}_t$ at position $t$ in a caption. The authors give the output word probability in 3.1.2 eq (7) as $p(\boldsymbol{y}_t | \boldsymbol{a}, \boldsymbol{y}_1^{t-1}) \propto exp(\boldsymbol{L}_o(\boldsymbol{Ey}_{t-1} + \boldsymbol{L}_h\boldsymbol{h}_t + \boldsymbol{L}_z\boldsymbol{\hat{z}}_t))$, where $\boldsymbol{L}_o$, $\boldsymbol{L}_h$, and $\boldsymbol{L}_z$ are linear learned layers initialized randomly.
\paragraph{}
I interpreted the part with "$\propto exp(\cdot)$" as a softmax final activation layer.
\paragraph{}
I implemented this deep output layer with very little modification, except that I added dropout with low probability (0.1) before each linear layer.

\section{Training}
\subsection{Overall}
\paragraph{}
The model was trained end-to-end by having the model predict a caption for any given image (whose features had already been extracted by the Encoder), and then comparing it to a ground truth caption for that image.
\paragraph{}
Each caption was converted to a vector representation using word-to-index as described in \ref{subsubsec:word2idx}. Then, each index was converted to a one-hot representation in order to compare it to the likelihood output of the Decoder.
\paragraph{}
The model trained for 11600 mini-batches on a nVidia Geforce GTX 1070, which took around a full day of training.
\subsection{Training time mini-batch selection}
\paragraph{}
Per 4.3 from the original paper, the authors implemented a trick to speed up training.
When training on a mini-batch, the time it takes is proportional to the length of the longest caption in the minibatch.
Therefore, if a minibatch contained some longer captions and some shorter ones, this would have been wasteful because the model would finish training on the shorter ones before the longer ones.
\paragraph{}
To remedy this, the authors categorized each caption by length, and trained on randomly sampled mini-batches with captions all of the same length.
The length itself was randomly sampled.
\paragraph{}
I implemented this mini-batch selection strategy. I chose mini-batch sizes of 48 captions.
\paragraph{}
In addition, I found that some caption lengths are more common than others eg. in the Flickr8k dataset, there are 3317 captions of length 9 but only 125 captions of length 3.
The authors did not specify how likely each length was to be chosen, so I assume the lengths were selected with uniform distribution.
However, this led to an issue where each caption with an infrequent length (eg. length 3, which only has a tiny pool to sample captions from) would be far more likely to be chosen for training than the captions with a frequent length (eg. length 9, which has a large pool of captions).
\paragraph{}
Instead of choosing the length with a uniform distribution, I assigned each length a probability proportional to the number of captions with that length in the training dataset. That way, every caption had roughly the same probability of being selected for training at any given step.

\subsection{Loss function and optimizer}
\paragraph{}
I implemented the loss function described in 4.2 eq (14) from the original paper: the loss was the negative log likelihood.
Specifically, I used the Pytorch implementation of cross entropy loss and omitted the final softmax layer, which (according to documentation\cite{pytorch_crossentropy}) is equivalent to using a final softmax layer combined with a negative log likelihood.
\paragraph{}
For the optimizer, I arbitrarily chose ADAM\cite{adam}. Again, I simply used the implementation supplied by Pytorch. I used a learning rate hyperparameter value of 0.00005.

\subsubsection{Doubly stochastic regularization}
\paragraph{}
I implemented the doubly stochastic attention/regularization described in 4.2.1 from the original paper. I chose a value of 0.01 for $\lambda$.

\subsection{Regularization}
\paragraph{}
As with the original paper, I implemented the regularization strategies of dropout (per 4.3 from the original paper) and doubly stochastic regularization (per 4.2.1 from the original paper).
the original paper never mentioned batch normalization, but I used it in the MLPs which I used to initialize my LSTM states.
For more details, see \ref{subsec:initMlp}.
I chose not to use batch normalization for the LSTM because \cite{lstm_bnorm} said that batch normalization for recurrent neural networks (of which an LSTM is a special type) requires special care to use properly.%, and I ran out of time to implement it.
\paragraph{}
I didn't quite use early stopping (per 4.3 from the original paper), because my model never ended up reaching a near-perfect training loss, while my evaluation scores never hit an optimum and proceeded to become significantly worse.
For more details on validation, see \ref{subsec:validation} %TODO make sure the reader can tell the difference between references for this paper vs references towards the original paper.
\subsection{Validation}
\label{subsec:validation}
\paragraph{}
As mentioned in \ref{subsec:meteor}, I exclusively used METEOR as an evaluation metric.
Thus, during training, I validated my model using METEOR by evaluating it on the entire "dev" split of the Flickr8k dataset every 100 training mini-batches.
If the newly trained model obtained a better score than any previous version, it was saved for later testing, overwriting any previous bests.

\section{Evaluation}
\subsection{METEOR score}
\paragraph{}
As mentioned in \ref{subsec:meteor}, I chose only to use the METEOR metric and only the Flickr8k dataset. Thus, I only have one score to evaluate.
\paragraph{}
My model achieved a METEOR score of 14.81 on the Flickr8k "test" split. This score is significantly lower than the equivalent result reported in Table 1 from the original paper of 18.93. It is also lower than that of all other models compared in the original paper; the lowest of which earned 16.88.

\section{Analysis and Speculation}
\paragraph{}
I have no solid explanation for why my model performed so much poorer than that of the original paper. The following are some potential reasons:
\begin{itemize}
	\item I spent barely any time tuning my model's hyperparameters. Rather than copying the canonical values from the authors' code (because these values were not given in the paper), I chose to try to guess good values. Perhaps a more careful tuning of the hyperparameters would produce better results. For example, the LSTM dimensionality (which I arbitrarily chose) could be larger, or the various MLPs (for which I guessed architectures) could be made deeper or otherwise more powerful.
	\item I never allowed my model to train to the point where it started to overfit because I didn't have time. In fact, I'm not even sure if my model is powerful enough to overfit, because the training loss seemed to fluctuate around 3.5 without showing signs of dropping significantly lower and staying there. It's possible that more training time or a better selection of hyperparameters would have allowed my model to do better. Alternatively, perhaps my dropout probabilities were set too high. I don't believe I set my learning rate too high (this is a common cause for fluctuations\cite{learning_rate}); a value of 0.00005 seems quite low compared to Pytorch's default value of 0.001.
	\item It's possible that, had I limited my search only to shorter captions, my model would have been trained on more concise captions.
	\item Perhaps my choice to use hidden-to-hidden transitions may have harmed training by forcing the back-propagation process to go through more hyperbolic tangent layers. Too many of these layers can cause the "vanishing gradient" problem, which is one of the reasons the ReLU activation function is often preferred over the hyperbolic tangent\cite{relu}.
	\item Despite my best efforts in applying proper software engineering techniques, perhaps I missed a bug in my code.
\end{itemize}
\subsection{Common failure modes}
\label{subsec:modes}
\paragraph{}
This section describes some of the common failure modes I've observed during evaluation. The examples in this section were all generated by my highest-validating model on images from the test split of Flickr8k.
\paragraph{}
Overall, the sentences of the generated captions don't tend to make grammatical sense.
On the positive side though, they tend to use correct words to describe the salient content of the image.
For example, if the image features a dog, the model will generate a caption with the word "dog".
\subsubsection{Repetition of filler words}\begin{figure}[ht]
\centering
\includegraphics[width=0.5\textwidth]{54501196_a9ac9d66f2}
\caption{a man is a a a a a a (according to my model)}
\label{fig:hiker}
\end{figure}
\paragraph{}
I have noticed that my model often generates captions which simply repeat words like "a" over and over. For example, see figure \ref{fig:hiker}, where my model decided that the best caption for that image was "a man is a a a a a a". \footnote{
The five ground truth captions for that image are as follows:
\begin{itemize}
	\item A backpacker in the mountains using his hiking stick to point at a glacier
	\item A backpacker points to the snow-capped mountains as he stands on a rocky plain
	\item A hiker is pointing towards the mountains
	\item A hiker poses for a picture in front of stunning mountains and clouds
	\item A man with a green pack using his pole to point to snowcapped mountains
\end{itemize}
}
\paragraph{}
I believe this happens when my model simply has no idea what words to choose.
Words like "a" appear very frequently in captions, so they probably serve as some sort of default filler word when the classifier doesn't have anything better to write.

\subsubsection{Repetition of the content}\begin{figure}[ht]
\centering
\includegraphics[width=0.3\textwidth]{544576742_283b65fa0d}
\caption{a man in a climbing rock climbing climbing rock (according to my model)}
\label{fig:climber}
\end{figure}

\begin{figure}[ht]
\centering
\includegraphics[width=0.5\textwidth]{925491651_57df3a5b36}
\caption{a brown dog dog is through the (according to my model)}
\label{fig:dog_dog}
\end{figure}
\paragraph{}
Though my model often correctly recognizes the content of an image, it often repeats the phrasing representing the content. For example, see figures \ref{fig:climber} and \ref{fig:dog_dog}, where "climbing" and "dog" were repeated (respectively). I believe this happens because the words which correctly represent the content of the image often appear in different positions within the ground truth labels. For example, for the image in figure \ref{fig:dog_dog}, the first ground truth caption contains the word "dog" as the second word, but the second caption contains the word "dog" as the third word. My model doesn't seem to understand how to generate gramatically correct sentences, so it may have been influenced by the multiple ground truths to put the word "dog" as \emph{both} the second and third words.
\footnote{
The five ground truth captions for the rock climbing image are as follows:
\begin{itemize}
	\item A rock climber repels (sic) off a rocky wall
	\item A woman climbing a rock cliff
	\item A woman in jeans rock climbing
	\item The woman is holding onto a huge rock and has red string around her
	\item Woman goes rock climbing
\end{itemize}
}
\footnote{
The five ground truth captions for the dog image are as follows:
\begin{itemize}
	\item A dog leaps out of the water
	\item a small dog jumping threw (sic) some a lake
	\item A tan dog jumps into water
	\item A white dog is splashing through the water
	\item Dog jumping out of water in a lake
\end{itemize}
}

\subsection{Success cases}
\begin{figure}[ht]
\centering
\subfloat[a boy splashes in water pool]{\includegraphics[width=0.25\textwidth]{539751252_2bd88c456b}}
\qquad
\subfloat[a man standing in a mountain]{\includegraphics[width=0.25\textwidth]{541063517_35044c554a}}
\qquad
\subfloat[two dogs are on the beach]{\includegraphics[width=0.25\textwidth]{3585487286_ef9a8d4c56}}
\caption{Some relatively successful cases}
\label{fig:success}
\end{figure}
\paragraph{}
Despite the failure modes mentioned in \ref{subsec:modes}, which appear in almost every generated caption, I'm proud to showcase some (albeit rare) cases in which the model has been relatively successful at generating a meaningful caption for an image. These cases are shown in figure \ref{fig:success}. The generated captions make sense and correctly describe the content of the images.
\paragraph{}
As with \ref{subsec:modes}, all examples were generated by my best-validating model. These images all come from the test set, meaning my model was not trained or validated on them.

\section{Conclusion}
\paragraph{}
I have reimplemented significant parts of the model in the paper Show, Attend, and Tell.
In some areas, I had to improvise when the original paper was less-than-perfectly explicit about an implementation detail.
My implementation failed to achieve the same results reported in the original paper, possibly because I made suboptimal guesses for my hyperparameters.
Perhaps, with more time to tune them and train my model, I could have done better.

\begin{thebibliography}{99}
\bibitem{paper}
  Xu, Kelvin, Ba, Jimmy Lei, Kiros, Ryan, Cho, Kyunghyun, Courville, Aaron, Salakhutdinov, Ruslan, Zemel, Richard S., and Bengio, Yoshua.
  Show, Attend and Tell: Neural Image Caption
Generation with Visual Attention.
  Université de Montréal, University of Toronto
  February 2015
  
\bibitem{rnn}
 Mao, Junhua, Xu, Wei, Yang, Yi, Wang, Jiang, and Yuille, Alan.
 Deep captioning with multimodal recurrent neural networks
(m-rnn). December 2014.

\bibitem{lstm_origin}
Hochreiter, S. and Schmidhuber, J.
Long short-term memory. \emph{Neural Computation}, 9(8):1735–1780, 1997.

\bibitem{lstm_paper1}
Vinyals, Oriol, Toshev, Alexander, Bengio, Samy, and Erhan, Dumitru.
Show and tell: A neural image caption generator. November 2014.

\bibitem{lstm_paper2}
Donahue, Jeff, Hendricks, Lisa Anne, Guadarrama, Segio, Rohrbach, Marcus, Venugopalan, Subhashini, Saenko, Kate, and Darrell, Trevor.
Long-term recurrent convolutional networks for visual recognition and description. November 2014.

\bibitem{soft_attention}
Bahdanau, Dzmitry, Cho, Kyunghyun, and Bengio, Yoshua.
Neural machine translation by jointly learning to align and translate. September 2014.

\bibitem{flickr8k}
Hodosh, Micah, Young, Peter, and Hockenmaier, Julia.
Framing image description as a ranking task: Data, models and evaluation metrics.
\emph{Journal of Artificial Intelligence Research}, pp.
853–899, 2013.

\bibitem{flickr30k}
Young, Peter, Lai, Alice, Hodosh, Micah, and Hockenmaier, Julia.
From image descriptions to visual denotations: New similarity metrics for semantic inference over event descriptions. \emph{TACL},
2:67–78, 2014.

\bibitem{coco}
Lin, Tsung-Yi, Maire, Michael, Belongie, Serge, Hays, James, Perona, Pietro, Ramanan, Deva, Dollar, Piotr, and Zitnick, C Lawrence. Microsoft coco: Common objects in context. In
\emph{ECCV}, pp. 740–755. 2014.

\bibitem{pytorch}
Pytorch.
\url{http://pytorch.org/}
Accessed April 30, 2018.

\bibitem{bleu}
Papineni, Kishore, Roukos, Salim, Ward, Todd, and Zhu, Wei-Jing.
BLEU: a Method for Automatic Evaluation of Machine Translation.
In \emph{ACL}, pp. 311-318, July 2002.

\bibitem{meteor}
Denkowski, Michael and Lavie, Alon. Meteor universal: Language specific translation evaluation for any target language.
In \emph{Proceedings of the EACL 2014 Workshop on Statistical Machine Translation}, 2014.

\bibitem{vgg}
Simonyan, K. and Zisserman, A. Very deep convolutional networks for large-scale image recognition. \emph{CoRR},
abs/1409.1556, 2014.

\bibitem{pytorch_vgg}
PyTorch torchvision models documentation.
\url{http://pytorch.org/docs/stable/torchvision/models.html}
Accessed April 30, 2018

\bibitem{bnorm}
Ioffe, Sergey and Szegedy, Christian.
Batch Normalization: Accelerating Deep Network Training by Reducing Internal Covariate Shift.
In \emph{CoRR}, 2015.

\bibitem{arctic}
Xu, Kelvin.
arctic-captions.
\url{https://github.com/kelvinxu/arctic-captions}
Accessed April 30, 2018.

\bibitem{imagenet}
Deng, Jia, Dong, Wei, Socher, Richard, Li, Li-Jia, Li, Kai, and Fei-Fei, Li.
ImageNet: A Large-Scale Hierarchical Image Database.
In \emph{Proc. CVPR}, 2009.

\bibitem{init}
A Beginner’s Guide to Recurrent Networks and LSTMs.
\url{https://deeplearning4j.org/lstm.html}. Accessed April 30, 2018.

\bibitem{dropout}
Srivastava, Nitish, Hinton, Geoffrey, Krizhevsky, Alex, Sutskever, Ilya, and Salakhutdinov, Ruslan.
Dropout: A simple way to prevent neural networks from overfitting. \emph{JMLR}, 15, 2014.

\bibitem{embedding}
Deep Learning \#4: Why You Need to Start Using Embedding Layers.
\url{https://towardsdatascience.com/deep-learning-4-embedding-layers-f9a02d55ac12}. Accessed April 30, 2018.

\bibitem{dropout_input}
Zaremba, Wojciech, Sutskever, Ilya, and Vinyals, Oriol.
Recurrent neural network regularization. \emph{arXiv preprint arXiv:1409.2329}, September 2014.

\bibitem{h2h}
Pascanu, Razvan, Gulcehre, Caglar, Cho, Kyunghyun, and Bengio, Yoshua.
How to construct deep recurrent neural networks.
In \emph{ICLR}, 2014.

\bibitem{pytorch_crossentropy}
Pytorch documentation on Cross Entropy Loss.
\url{http://pytorch.org/docs/stable/nn.html#torch.nn.CrossEntropyLoss}
Accessed April 30, 2018.

\bibitem{adam}
Kingma, Diederik P. and Ba, Jimmy.
Adam: A Method for Stochastic Optimization.
December
2014.

\bibitem{lstm_bnorm}
Cooijmans, Tim, Ballas, Nicolas, Laurent, César, Gülçehre, Çağlar, and Courville, Aaron.
Recurrent Batch Normalization.
In \emph{CoRR abs/1603.09025}, 2016.

\bibitem{learning_rate}
CS231n Convolutional Neural Networks for Visual Recognition.
\url{http://cs231n.github.io/neural-networks-3/}. Accessed April 30, 2018.

\bibitem{relu}
Glorot, Xavier, Bordes, Antoine, and Bengio, Yoshua.
Deep Sparse Rectifier Neural Networks.
In \emph{Proc. JMLR: W\&CP 15}. 2011.
\end{thebibliography}
\end{document}