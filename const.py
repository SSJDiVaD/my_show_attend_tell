"""Just some constants"""
import numpy
import random
import torch

import utils

USE_CUDA = True
PRINT_TIMES = True  # whether the utils.Timer class should print its timings
EPS = 10 ** -7  # just a really small number

# params from the paper --------------------------------------------------------
L = 196  # from 4.3, number of attention locations a_i
D = 512  # from 4.3, dimensionality of each attention vector a_i
m = 1024  # from 3.1.2, embedding dimensionality. Arbitrary choice (512?)
n = 1536  # from 3.1.2, LSTM dimensionality. Arbitrary choice (1024?)
# K = None  # from 3.1.1, vocabulary size. Excluded because automatically generated

# preprocessing-related --------------------------------------------------------
PREPRO_SHORTEST_SIDE = 256
PREPRO_FINAL_SIDE = 224
PREPRO_IMAGE_SHAPE = (224, 224, 3)
PREPRO_BATCH_SIZE = 100  # batch size for extracting feature vectors from vggnet.
PREPRO_LIMIT = 10000  # for testing purposes, only do these many images per split. Set to None for unlimited
VGG_USE_BN = True
PREPRO_FILENAME = 'image/{0}_preprocessed.npy'  # arg will be replaced by 'train' or 'val'

# flickr 8k related ------------------------------------------------------------
C_min = 3  # we will ignore any caption shorter than this.
C_max = 20  # we will ignore any caption longer than this.
FLICKR8K_DIR = 'flickr8k'
FLICKR8K_TRAIN_FILENAME = FLICKR8K_DIR + '/Flickr_8k.trainImages.txt'
FLICKR8K_DEV_FILENAME = FLICKR8K_DIR + '/Flickr_8k.devImages.txt'
FLICKR8K_TEST_FILENAME = FLICKR8K_DIR + '/Flickr_8k.testImages.txt'
FLICKR8K_CAPS_FILENAME = FLICKR8K_DIR + '/Flickr8k.token.txt'
FLICKR8K_IMG_DIR = FLICKR8K_DIR + '/Flicker8k_Dataset'
FLICKR8K_PREPRO_DIR = 'flickr8k_prepro'
FLICKR8K_PREPRO_CAPS_DIR = FLICKR8K_PREPRO_DIR + '/caps'
# args replaced by split, length of caption
FLICKR8K_CAPS_SORTED_FILENAME = FLICKR8K_PREPRO_CAPS_DIR + '/caps_{0}_{1}.txt'
# arg replaced by split
FLICKR8K_LENGTHS_FILENAME = FLICKR8K_PREPRO_CAPS_DIR + '/lengths.txt'
FLICKR8K_WORD2IDX_FILENAME = FLICKR8K_PREPRO_CAPS_DIR + '/word2idx.txt'
FLICKR8K_DEV_CAPS_FILENAME = FLICKR8K_PREPRO_CAPS_DIR + '/caps_dev.txt'
FLICKR8K_TEST_CAPS_FILENAME = FLICKR8K_PREPRO_CAPS_DIR + '/caps_test.txt'
FLICKR8K_FEATURES_FILENAME = FLICKR8K_PREPRO_DIR + '/.flickr8k_preprocessed.npy'

# neural net related -----------------------------------------------------------
# minibatch size for training. The paper used batch size 64, but I might have to
# use a smaller number because my network is probably bigger.
# 28 is too big, and 27 works.
TRAIN_MINIBATCH_SIZE = 48
# TRAIN_NUM_BATCHES = 10000  # number of batches on which to train
TRAIN_LEARNING_RATE = 0.00005
TRAIN_DOUBLY_STOCHASTIC_LAMBDA = 0.01
TRAIN_DIR = 'trained_models'
TRAIN_LATEST_FILENAME = TRAIN_DIR + '/latest.pt'  # I don't know if PyTorch recommends a canonical extension
TRAIN_SECOND_LATEST_FILENAME = TRAIN_DIR + '/latest_2.pt'
TRAIN_BEST_FILENAME = TRAIN_DIR + '/best.pt'
# 6000 training images * 5 captions per image = 30 000 captions
# it will take at least (n training captions / train batch size  ~= 1000)
# batches before we have a chance of iterating through every caption (ie. one
# epoch)
TRAIN_EVAL_INTERVAL = 100  # number of batches between cross-validation runs

# approx number of neurons in each of the the hidden layer(s) of the basic MLP
# used to initialize c_0 and h_0 for the LSTM. Arbitrary choice.
BASIC_MLP_HIDDEN_SIZE = 1024
F_ATT_HIDDEN_SIZE = 512  # number of neurons in hidden layers of f_att. Arbitrary choice.
F_BETA_HIDDEN_SIZE = utils.round_int(n / 2)
F_BETA_DROPOUT_PROB = 0.1
MLP_DROPOUT_PROB = 0.1
F_ATT_DROPOUT_PROB = 0.2
LSTM_DROPOUT_PROB = 0.2
OUTPUT_DROPOUT_PROB = 0.3

EVAL_METEOR_DIR = 'meteor-1.5'  # The location of the extracted METEOR directory
EVAL_METEOR_JAR = 'meteor-1.5.jar'
EVAL_METEOR_TIMEOUT = 10  # number of seconds to wait for METEOR to respond
EVAL_MINIBATCH_SIZE = 64  # I think evaluation doesn't require the same memory
# as training, so this number can be bigger
EVAL_DIR = 'eval'
EVAL_HYPO_FILENAME = EVAL_DIR + '/tmp/hypotheses.txt'


# don't modify this ------------------------------------------------------------
USE_CUDA = USE_CUDA and torch.cuda.is_available()
if USE_CUDA:
    print('using CUDA')
else:
    print('NOT using CUDA')
# set seeds to consistent value for repeatability
random.seed(0)
numpy.random.seed(0)
torch.manual_seed(0)
